// 1. JavaScript-Konstanten für HTML-Elemente
const buttonWuerfeln = document.querySelector('#buttonWuerfeln');
const td2 = document.querySelector('#td2');
const td3 = document.querySelector('#td3');
const td4 = document.querySelector('#td4');
const td5 = document.querySelector('#td5');
const td6 = document.querySelector('#td6');
const td7 = document.querySelector('#td7');
const td8 = document.querySelector('#td8');
const td9 = document.querySelector('#td9');
const td10 = document.querySelector('#td10');
const td11 = document.querySelector('#td11');
const td12 = document.querySelector('#td12');
const tdProzent = document.querySelector('#tdProzent'); // Neue Spalte für Prozentsatz

// 2. Daten
const anzahlen = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// 3. Funktionsdefinitionen
function reset() {
    for (let i = 0; i < anzahlen.length; i++) {
        anzahlen[i] = 0;
    }
}

function wuerfeln() {
    // 1000x würfeln
    for (let i = 0; i < 6000; i++) {
        const wuerfel1 = Math.floor(Math.random() * 6) + 1;
        const wuerfel2 = Math.floor(Math.random() * 6) + 1;
        const augensumme = wuerfel1 + wuerfel2;

        anzahlen[augensumme - 2]++;
    }
}

function ausgeben() {
    // Anzahlen ausgeben
    td2.textContent = anzahlen[0];
    td3.textContent = anzahlen[1];
    td4.textContent = anzahlen[2];
    td5.textContent = anzahlen[3];
    td6.textContent = anzahlen[4];
    td7.textContent = anzahlen[5];
    td8.textContent = anzahlen[6];
    td9.textContent = anzahlen[7];
    td10.textContent = anzahlen[8];
    td11.textContent = anzahlen[9];
    td12.textContent = anzahlen[10];

    // Prozentuale Anzahlen berechnen und ausgeben
    const gesamtWuerfe = 6000;
    for (let i = 0; i < anzahlen.length; i++) {
        const prozent = (anzahlen[i] / gesamtWuerfe) * 100;
        document.querySelector(`#td${i + 2}Prozent`).textContent = prozent.toFixed(2) + '%';
    }
}

function buttonWuerfelnClick() {
    reset();
    wuerfeln();
    ausgeben();
}

// 4. Eventlistener registrieren
buttonWuerfeln.addEventListener('click', buttonWuerfelnClick);

// 5. Start
reset();
ausgeben();
